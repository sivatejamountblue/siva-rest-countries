import React, { Component } from "react";

import { Link } from "react-router-dom";

class Main extends Component {
  state = {
    data: [],
    isLoaded: false,
    userInput: "",
  };
  handleChange = (e) => {
    let inputValue = e.target.value;
    this.setState({
      userInput: inputValue,
    });
    console.log(this.state.userInput);
  };
  handleFilter = (e) => {
    let region = e.target.value.toLowerCase();
    console.log(region);
    if (region === "filter by region") {
      fetch(`https://restcountries.com/v3.1/all`)
        .then((resp) => resp.json())
        .then((resp) => {
          this.setState({ data: resp, isLoaded: true });
        });
    } else {
      fetch(`https://restcountries.com/v3.1/region/${region}`)
        .then((resp) => resp.json())
        .then((resp) => {
          this.setState({ data: resp, isLoaded: true });
        });
    }
  };

  displayCountries = () => {
    const filteredCountries = this.state.data.filter((each) =>
      each.name.common.toLowerCase().includes(this.state.userInput)
    );
    if (filteredCountries) {
      return filteredCountries;
    } else {
      return [];
    }
  };
  componentDidMount = () => {
    fetch(`https://restcountries.com/v3.1/all`)
      .then((resp) => resp.json())
      .then((resp) => {
        console.log(resp.length);
        console.log(resp[0]);
        console.log(resp[0].name);
        console.log(resp[0].name.official);
        this.setState({ data: resp, isLoaded: true });
      });
  };
  render() {
    const { isLoaded } = this.state;
    const displayCountries = this.displayCountries();
    console.log(this.state);
    return (
      <div>
        <div className="d-flex justify-content-between align-items-center my-3">
          <div className= {this.props.theme?"d-flex input-container-dark justify-content-center align-items-center":"d-flex input-container justify-content-center align-items-center"}>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="16"
              height="16"
              fill="currentColor"
              className="bi bi-search mr-2 ml-2"
              viewBox="0 0 16 16"
            >
              <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z" />
            </svg>
            <input
              className= {this.props.theme?"input-field-style-dark":"input-field-style"}
              onChange={this.handleChange}
              placeholder="Search for a country..."
              type="text"
            />
          </div>

          <select
            className= {this.props.theme?"drop-down-style-dark":"drop-down-style"}
            onChange={this.handleFilter}
            id="dropdown"
            ref={(input) => (this.menu = input)}
          >
            <option value="Filter by region">Filter by region</option>
            <option value="Africa">Africa</option>
            <option value="Americas">Americas</option>
            <option value="Asia">Asia</option>
            <option value="Europe">Europe</option>
            <option value="Oceania">Oceania</option>
          </select>
        </div>
        <div className="country-container d-flex flex-wrap m-2">
          {isLoaded
            ? Array.from(displayCountries).map((each) => (
                <div key={each} className="each-country m-3">
                  <img className="country-name" src={each.flags.png} alt="" />
                  <div className= {this.props.theme?"country-info-dark":"country-info"}>
                    <h3>
                      {" "}
                      <Link to={`/${each.name.common.toLowerCase()}`}>
                        {each.name.common}
                      </Link>
                    </h3>
                    <p>Population: {each.population}</p>
                    <p>Region: {each.region}</p>
                    <p>Capital: {each.capital}</p>
                  </div>
                </div>
              ))
            : "loading..."}
        </div>
      </div>
    );
  }
}

export default Main;
