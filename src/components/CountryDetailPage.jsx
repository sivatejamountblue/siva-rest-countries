import React, { Component } from "react";
import {Link} from 'react-router-dom'
class CountryDetailPage extends Component {
  state = {
    data: [],
    isLoaded: false,
  };
  componentDidMount = () => {
    let country = this.props.match.url
    let url = `https://restcountries.com/v3.1/name${country}`
    console.log(country)
    console.log(url)
    fetch(url)
      .then((resp) => resp.json())
      .then((resp) => {
        this.setState({ data: resp, isLoaded: true });
      });
  };
  render() {
    console.log(this.props.match.url)
    console.log(this.props.theme)
    const { data, isLoaded } = this.state;
    return (
      <div>
        {isLoaded
          ? Array.from(data).map((each) => (
              <div key={each} className= {this.props.theme?"country-detail-page-container-dark justify-content-center align-items-center":"country-detail-page-container justify-content-center align-items-center"}>
                  <Link to='/' ><button className={this.props.theme?"m-5 country-detail-page-button-dark":"m-5 country-detail-page-button"}>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="16"
                    height="16"
                    fill="currentColor"
                    className="bi bi-arrow-left"
                    viewBox="0 0 16 16"
                  >
                    <path
                      d="M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z"
                    />
                  </svg>
                  Back
                </button></Link>
                
                <div className="each-country-detail-page d-flex justify-content-around m-3">
                  <div>
                    <img className="countrydetail-page-image" src={each.flags.png} alt="" />
                  </div>
                  <div className={this.props.theme?"country-info-dark":"country-info"}>
                    <div>
                      <h3 className="country-detail-page-country-name">{each.name.common}</h3>
                    </div>
                    <div className="country-detail-page-info">
                      <p> <span className={this.props.theme?"country-detail-page-sub-titile-dark":"country-detail-page-sub-titile"}>Native name: </span>{each.name.official}</p>
                      <p> <span className={this.props.theme?"country-detail-page-sub-titile-dark":"country-detail-page-sub-titile"}>Population: </span> {each.population}</p>
                      <p> <span className={this.props.theme?"country-detail-page-sub-titile-dark":"country-detail-page-sub-titile"}>Region: </span>{each.region}</p>
                      <p> <span className={this.props.theme?"country-detail-page-sub-titile-dark":"country-detail-page-sub-titile"}>Sub Region: </span>{each.subregion}</p>
                      <p> <span className={this.props.theme?"country-detail-page-sub-titile-dark":"country-detail-page-sub-titile"}>Capital: </span>{each.capital}</p>
                      <p> <span className={this.props.theme?"country-detail-page-sub-titile-dark":"country-detail-page-sub-titile"}>Top Level Domain: </span>{each.tld}</p>
                      <p> <span className={this.props.theme?"country-detail-page-sub-titile-dark":"country-detail-page-sub-titile"}>Currencies: </span>{ Object.values(each.currencies)[0].name}</p>
                      <div className="d-flex">
                      <span className={this.props.theme?"country-detail-page-sub-titile-dark":"country-detail-page-sub-titile"}>Languages:</span>
                        {Object.values(each.languages).map((each) => {
                          return <p key={each} className="ml-2">{each}</p>;
                        })}
                      </div>
                    </div>
                    <div className="d-flex ">
                    <span className={this.props.theme?"country-detail-page-sub-titile-dark":"country-detail-page-sub-titile"}>Border Countries:</span>{" "}
                      {
                        each.borders ? each.borders.map((each) => {
                          return <p key={each} className="ml-2 border-countries-style">{each} </p>;
                        }) : null
                      }
                    </div>
                  </div>
                </div>
              </div>
            ))
          : "loading..."}
      </div>
    );
  }
}

export default CountryDetailPage;
