import React, { Component } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import "./App.css";
import Header from "./components/Header";
import Main from "./components/Main";
import CountryDetailPage from "./components/CountryDetailPage";

class App extends Component {
  state= {
    theme: false
  }
  handleThemeChange = () =>{
    this.setState({
      theme: !this.state.theme
    })
    console.log(this.state)
  }
  render() {
    return (
      <div className= {this.state.theme?"container-dark":"container-fluid"}>
        <BrowserRouter>
          <Header theme = {this.state.theme} handleThemeChange = {this.handleThemeChange}/>
          <Switch>
            <Route exact path="/" component={() => (<Main theme = {this.state.theme} />)} />
            <Route exact path="/:countryName" component={(props) => <CountryDetailPage theme = {this.state.theme}{...props} />} />
            
          </Switch>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
